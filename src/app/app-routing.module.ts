import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { InscriptionComponent } from './modules/membre/components/inscription/inscription.component';
import { LoginDialogComponent } from './modules/membre/components/login-dialog/login-dialog.component';
import { MainComponent } from './v2/components/main/main.component';
import { ParrainerComponent } from './v2/components/parrainer/parrainer.component';
import { InscriptionComponent } from './v2/components/inscription/inscription.component';
import { CreateBrancheComponent } from './v2/components/create-branche/create-branche.component';
import { AccueilSectionComponent } from './v2/components/accueil-section/accueil-section.component';
import { AboutSectionComponent } from './v2/components/about-section/about-section.component';
import { LoginComponent } from './v2/components/login/login.component';
import { MonCompteSectionComponent } from './v2/components/mon-compte-section/mon-compte-section.component';
import { UserConnectedGuard } from './guards/user-connected.guard';
import { MainAdaminComponent } from './v2/components/main-adamin/main-adamin.component';
import { SearchMembreComponent } from './modules/administrateur/components/search-membre/search-membre.component';
import { RapportAgentComponent } from './modules/administrateur/components/rapport-agent/rapport-agent.component';
import { LoginAdaminComponent } from './v2/components/login-adamin/login-adamin.component';
import { PaiementComponent } from './v2/components/paiement/paiement.component';
import { RetraitComponent } from './v2/components/retrait/retrait.component';
import { PaiementAdminComponent } from './v2/components/paiement-admin/paiement-admin.component';
import { TransfertComponent } from './v2/components/transfert/transfert.component';
import { DashboardComponent } from './v2/components/dashboard/dashboard.component';
import { ApprovisionnementComponent } from './v2/components/approvisionnement/approvisionnement.component';
import { RapportApproComponent } from './v2/components/rapport-appro/rapport-appro.component';
import { ApproAgentComponent } from './v2/components/appro-agent/appro-agent.component';
import { ActivationManagerComponent } from './v2/components/activation-manager/activation-manager.component';
import { MesActivationsComponent } from './v2/components/mes-activations/mes-activations.component';
import { MesPaiementsComponent } from './v2/components/mes-paiements/mes-paiements.component';
import { ConfigMembreComponent } from './v2/config-membre/config-membre.component';
import { ConfigRoleMembreComponent } from './v2/config-role-membre/config-role-membre.component';


const routes: Routes = [
  {
    path: '', component: MainComponent, canActivate:[UserConnectedGuard],
    children:[
      {
        path: '', component: AccueilSectionComponent
        
      },
      {
        path: 'login', component: LoginComponent
      },
      {
        path: 'inscription', component: InscriptionComponent
      },
      {
        path: 'branche', component: CreateBrancheComponent
      },
      {
        path: 'about', component: AboutSectionComponent
      },
      {
        path: 'parrainer', component: ParrainerComponent
      },
      {
        path: 'mes-retraits', component: PaiementComponent
      },
      {
        path: 'retrait', component: RetraitComponent
      },
      {
        path: 'transfert', component: TransfertComponent
      },
      {
        path: 'activation', component: ActivationManagerComponent
      },
      {
        path: 'mes-activations', component: MesActivationsComponent
      },
      {
        path: 'mes-paiements', component: MesPaiementsComponent
      },
      {
        path: 'config', component: ConfigMembreComponent
      },
      {
        path: 'myaccount', component: MonCompteSectionComponent
      }
    ]
  },
  {
    path: 'admin',
    children:[
      {
        path: '', component: LoginAdaminComponent
      },
      {
        path:'main', component: MainAdaminComponent,
        children:[
          
          {
            path: 'search', component: SearchMembreComponent
          },
          {
            path: 'rapport', component: RapportAgentComponent
          },
          {
            path: 'appro', component: ApprovisionnementComponent
          },
          {
            path: 'appro-agence', component: ApproAgentComponent
          },
          {
            path: 'rapport-appro', component: RapportApproComponent
          },
          {
            path: 'paiement', component: PaiementAdminComponent
          },
          {
            path: 'change-role', component: ConfigRoleMembreComponent
          },
          {
            path: 'dashboard', component: DashboardComponent
          }
        ]
      }
    ]
  },
  // {
  //   path: 'dashboard', component: DashboardComponent
  // }
  
  // {
  //   path: 'login', component: LoginDialogComponent
  // },
  // {
  //   path: 'inscription', component: InscriptionComponent
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
