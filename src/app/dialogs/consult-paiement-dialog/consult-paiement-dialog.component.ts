import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Paiement } from 'src/app/models/paiement';
import { InputDialogComponent } from 'src/app/modules/shared-module/dialogs/input-dialog/input-dialog.component';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-consult-paiement-dialog',
  templateUrl: './consult-paiement-dialog.component.html',
  styleUrls: ['./consult-paiement-dialog.component.css']
})
export class ConsultPaiementDialogComponent implements OnInit {

  paiement: Paiement;

  constructor(
    private dialogRef: MatDialogRef<ConsultPaiementDialogComponent>,
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: AdministrateurServiceService,
    @Inject(MAT_DIALOG_DATA) data: Paiement
  ) { 
    this.paiement = data
  }

  ngOnInit(): void {
  }

  valider():  void{
    let dial = this.dialog.open<InputDialogComponent>(InputDialogComponent, 
      {
        data: 'Veuillez inserer votre mot de passe'
      });
      dial.afterClosed().subscribe(
        (password: string) =>{
          if(this.service.userConnected.password === password){
            this.service.effectuerPaiement(JSON.stringify(this.paiement)).then(
              res => {
                Utility.openSuccessDialog(this.dialog, res.error.errorDescription)
                this.dialogRef.close()
              }
            ).catch(
              error => {
                Utility.openErrorDialog(this.dialog, error)
                this.dialogRef.close();
              }
            )
          }else{
            Utility.openInfoDialog(this.dialog, 'Le mot de passe fournit est incorrect');
          }
        }
      )
  }

  annuler():  void{
    let dial = this.dialog.open<InputDialogComponent>(InputDialogComponent, 
      {
        data: 'Veuillez inserer votre mot de passe'
      });
      dial.afterClosed().subscribe(
        (password: string) =>{
          if(this.service.userConnected.password === password){
            this.service.annulerPaiement(JSON.stringify(this.paiement)).then(
              res => {
                Utility.openSuccessDialog(this.dialog, res.error.errorDescription)
                this.dialogRef.close()
              }
            ).catch(
              error => {
                Utility.openErrorDialog(this.dialog, error)
                this.dialogRef.close();
              }
            )
          }else{
            Utility.openInfoDialog(this.dialog, 'Le mot de passe fournit est incorrect');
          }
        }
      )
  }


}
