import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-information-dialog',
  templateUrl: './information-dialog.component.html',
  styleUrls: ['./information-dialog.component.css']
})
export class InformationDialogComponent implements OnInit {

  message: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) data: string
  ) { 
    this.message = data;
  }

  ngOnInit(): void {
  }

}
