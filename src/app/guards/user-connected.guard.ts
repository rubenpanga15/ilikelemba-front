import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppConfigService } from '../services/app-config.service';

@Injectable({
  providedIn: 'root'
})
export class UserConnectedGuard implements CanActivate {
  constructor(
    private router: Router,
    private appConfig: AppConfigService
    ){

    }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      this.verifyUser();
      return true;
      
  }

  verifyUser(): void{
    let userJson:string = sessionStorage.getItem(AppConfigService.USER_KEY);
      if(userJson !== 'undefined'){
        this.appConfig.userConnected = JSON.parse(userJson);
        this.appConfig.isConnected$.next(true);
      }
  }
  
}
