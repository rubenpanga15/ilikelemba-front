export class Administrateur {
    public id: number;
    public noms: string;
    public username: string;
    public password: string;
    public token: string;
    public fkProfilAcces: number;
    public statut: boolean;
    public dateCreat: Date;
    public code: string;
    public role: string;
    public approUSD: number;
    public approCDF: number;
}
