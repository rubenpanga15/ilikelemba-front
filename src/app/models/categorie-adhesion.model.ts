export class CategorieAdhesion {
    public id: number;
    public description: string;
    public montantAdhesion: number;
    public statut: boolean;
    public dateCreat: Date;
}
