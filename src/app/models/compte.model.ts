export class Compte {
    public id: number;
    public fkMembre: number;
    public soldeUSD: number;
    public soldeCDF: number;
    public approUSD: number;
    public approCDF: number;
    public statut: boolean;
    public dateCreat: Date;
}
