import { Administrateur } from "./administrateur.model";
import { MembreAdapter } from "./utilities/membre-adapter";

export class JournalAppro{
    id: number;
    type: string;
    fkProvenance: number;
    fkDestinateur: number;
    fkAgent: number;
    fkAgentDestinateur;
    montantcdf: number;
    montantusd: number;
    soldeCDF: number;
    soldeUSD: number;
    statut: boolean;
    dateCreat: Date

    destinateur: MembreAdapter;
    provenance: MembreAdapter;
    agent: Administrateur;
    agentDestinateur: Administrateur;
}