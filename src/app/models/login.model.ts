export class Login {
    public id: number;
    public username: string;
    public password: string;
    public token: string;
    public fkMembre: number;
    public statut: boolean;
    public dateCreat: Date;
    public role: string;
}
