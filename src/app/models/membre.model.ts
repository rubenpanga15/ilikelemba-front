export class Membre {
    public id: number;
    public nom: string;
    public postnom: string;
    public prenom: string;
    public sexe: string;
    public email: string;
    public telephone: string;
    public fkParrain: number;
    public fkArbre: string;
    public statut: boolean;
    public dateCreat: Date;
    public lieuNaissance: string;
    public dateNaissance: Date;
}
