export class Partage {
    id: number;
    g1: number;
    g2: number;
    g3: number;
    statut: boolean;
    dateCreat: Date
}
