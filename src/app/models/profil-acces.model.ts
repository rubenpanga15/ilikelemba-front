export class ProfilAcces {
    public id: number;
    public description: string;
    public statut: boolean;
    public dateCreat: Date;
}
