import { CategorieAdhesion } from '../categorie-adhesion.model';
import { Devise } from '../devise';
import { Membre } from '../membre.model';
import { Partage } from '../partage';
import { ParrainageAdapter } from './parrainage-adapter';

export class ArbreAdapter {
    code: string;
    fkGeniteur: string;
    fkCategorieAdheion: number;
    description: string;
    statut: boolean;
    dateCreat: Date;

    categorieAdhesion: CategorieAdhesion;
    devise: Devise;
    montant: number;
    partage: Partage

    geniteur: Membre

    g1: Membre[] = [];
    g2: Membre[] = [];
    g3: Membre[] = [];

    p1: ParrainageAdapter[] = [];
    p2: ParrainageAdapter[] = [];
    p3: ParrainageAdapter[] = [];
}
