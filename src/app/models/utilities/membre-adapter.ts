import { Login } from '../login.model';
import { Compte } from '../compte.model';
import { Arbre } from '../arbre.model';
import { ArbreAdapter } from './arbre-adapter';

export class MembreAdapter {
    public id: number;
    public nom: string;
    public postnom: string;
    public prenom: string;
    public telephone: string;
    public email: string;
    public fkParrain: number;
    public fkArbre: string;
    public statut: boolean;
    public dateCreat: Date;
    public sexe: string;
    public lieuNaissance: string;
    public dateNaissance: Date;

    public login: Login = new  Login();
    public compte: Compte = new Compte();
    public arbres: ArbreAdapter[] = []
}
