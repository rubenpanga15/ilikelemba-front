import { Membre } from '../membre.model';
import { ArbreAdapter } from './arbre-adapter';
import { MembreAdapter } from './membre-adapter';

export class ParrainageAdapter {
    id: number;
    membre: MembreAdapter;
    parrain: Membre;
    fkArbre: ArbreAdapter;
    statut: boolean;
    dateCreat: Date;
    dateActivation: Date;
    isAgent: boolean;
    
}
