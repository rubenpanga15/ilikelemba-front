import { Paiement } from '../paiement';
import { MembreAdapter } from './membre-adapter';
import { ParrainageAdapter } from './parrainage-adapter';

export class RapportAdapter{
    parrainages: ParrainageAdapter[];
    paiements: Paiement[];
    membres: MembreAdapter[];
}