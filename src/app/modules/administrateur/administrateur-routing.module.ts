import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { MainAdminNavComponent } from './components/main-admin-nav/main-admin-nav.component';
import { MainAdminComponent } from './components/main-admin/main-admin.component';
import { RapportAgentComponent } from './components/rapport-agent/rapport-agent.component';
import { SearchMembreComponent } from './components/search-membre/search-membre.component';


const routes: Routes = [
  {
    // path: 'admin',
    // children:[
    //   {
    //     path: '', component: LoginComponent
    //   },
    //   {
    //     path:'main', component: MainAdminComponent,
    //     children:[
    //       {
    //         path: 'search', component: SearchMembreComponent
    //       },
    //       {
    //         path: 'rapport', component: RapportAgentComponent
    //       }
    //     ]
    //   }
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrateurRoutingModule { }
