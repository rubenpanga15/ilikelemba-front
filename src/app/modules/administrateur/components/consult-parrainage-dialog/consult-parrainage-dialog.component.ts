import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ParrainageAdapter } from 'src/app/models/utilities/parrainage-adapter';
import { InputDialogComponent } from 'src/app/modules/shared-module/dialogs/input-dialog/input-dialog.component';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';

@Component({
  selector: 'app-consult-parrainage-dialog',
  templateUrl: './consult-parrainage-dialog.component.html',
  styleUrls: ['./consult-parrainage-dialog.component.css']
})
export class ConsultParrainageDialogComponent implements OnInit {

  parrainage: ParrainageAdapter;
  waiting: boolean = false;

  seeMore: boolean = false;

  constructor(
    private dialogRef: MatDialogRef<ConsultParrainageDialogComponent>,
    private appConfig: AppConfigService,
    private service: AdministrateurServiceService,
    @Inject(MAT_DIALOG_DATA) data: ParrainageAdapter,
    private dialog: MatDialog
  ) { 
    this.parrainage = data;
  }

  data ={
    id: undefined,
    fkAgent: this.service.userConnected.id
  }

  ngOnInit(): void {
    this.appConfig.waiting$.subscribe(
      (result : boolean) => {
        this.waiting = result;
      }
    )
  }

  activer(): void{
    this.data.id = this.parrainage.id

    let dialogRef = this.dialog.open<InputDialogComponent>(InputDialogComponent, 
      {
        data: 'Veuillez inserer votre mot de passe'
      });
      dialogRef.afterClosed().subscribe(
        (password: string) =>{
          if(this.service.userConnected.password === password){
            this.appConfig.onStartWaiting();
            this.service.activer(JSON.stringify(this.data)).then(
              (result: HttpDataResponse<boolean>) =>{
                console.log(result);
                this.appConfig.onStopWaiting();
                this.dialog.open(MessageDialogComponent, {
                  data: result.error.errorDescription
                })
                this.dialogRef.close(
                  {
                    data: 'OK'
                  }
                )
              }
            ).catch(error =>{
              this.appConfig.onStopWaiting();
              alert(error);
            });
          }else{
            this.dialog.open(MessageDialogComponent, {
              data: 'Le mot de passe fournit est incorrect'
            })
          }
        }
      )
    
    
  }

}
