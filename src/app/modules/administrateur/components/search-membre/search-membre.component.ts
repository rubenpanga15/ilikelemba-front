import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ParrainageAdapter } from 'src/app/models/utilities/parrainage-adapter';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { ConsultParrainageDialogComponent } from '../consult-parrainage-dialog/consult-parrainage-dialog.component';

@Component({
  selector: 'app-search-membre',
  templateUrl: './search-membre.component.html',
  styleUrls: ['./search-membre.component.css']
})
export class SearchMembreComponent implements OnInit, OnDestroy {

  subscription: Subscription

  constructor(
    private service: AdministrateurServiceService,
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private router: Router
  ) { 
    
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  data = {
    motclef: ''
  }

  ngOnInit(): void {
    if(!this.service.userConnected){
      this.router.navigate(['admin/']);
    }
   this.subscription= this.appConfig.waiting$.subscribe(
      (result: boolean) =>{
        this.waiting = result;
      }
    )
  }

  parrainages: ParrainageAdapter[] = []
  waiting: boolean = false;

  onSubmit(): void{
    this.parrainages = []
    this.appConfig.onStartWaiting();
    this.service.searchParrainage(JSON.stringify(this.data)).then(
      (result: HttpDataResponse<ParrainageAdapter[]>) => {
        this.appConfig.onStopWaiting();
        this.parrainages = result.response;
      }
    ).catch(error => {
      this.appConfig.onStopWaiting();
      alert(error)})
  }

  consultMembre(index: number): void{
    let dialogRef = this.dialog.open<ConsultParrainageDialogComponent, ParrainageAdapter>(ConsultParrainageDialogComponent,
      {
        width: '80%',
        height: '80%',
        data: this.parrainages[index]
      });

    dialogRef.afterClosed().subscribe(
      data =>{
        this.parrainages = [];
        if(data == 'OK'){
          
          this.router.navigate(['/admin/main']);
        }
      }
    )
  }

}
