import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-add-bottom-sheet',
  templateUrl: './add-bottom-sheet.component.html',
  styleUrls: ['./add-bottom-sheet.component.css']
})
export class AddBottomSheetComponent implements OnInit {

  constructor(
    private sheet: MatBottomSheetRef<AddBottomSheetComponent>
  ) { }

  ngOnInit(): void {
  }

close(): void{
  this.sheet.dismiss();
}

}
