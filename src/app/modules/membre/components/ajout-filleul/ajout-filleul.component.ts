import { Component, OnInit } from '@angular/core';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { NgForm } from '@angular/forms';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { Membre } from 'src/app/models/membre.model';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';
import { Util } from 'src/app/utilities/util';

@Component({
  selector: 'app-ajout-filleul',
  templateUrl: './ajout-filleul.component.html',
  styleUrls: ['./ajout-filleul.component.css']
})
export class AjoutFilleulComponent implements OnInit {

  membre: MembreAdapter = new MembreAdapter();
  arbres: ArbreAdapter[] = [];

  isChecked:boolean = false;

  constructor(
    private service: GestionFilleulService,
    private appConfig: AppConfigService,
    private router: Router,
    private dialog: MatDialog
  ) { 
    this.membre.fkParrain = this.appConfig.userConnected.fkParrain;
  }

  ngOnInit(): void {
    this.appConfig.verifyUser();
    // if(!this.appConfig.userConnected.nom){
    //   this.router.navigate(['main']);
    // }
    this.arbres = this.appConfig.userConnected.arbres;
    this.membre.sexe = 'M'
    
  }

  onSubmit():void{
    this.membre.fkParrain = this.appConfig.userConnected.id;
    console.log(this.membre)
    this.service.addFilleul(this.membre).then(
      (result: HttpDataResponse<Membre>) =>{
        for(let i = 0; i < this.appConfig.userConnected.arbres.length; i++){
          if(this.appConfig.userConnected.arbres[i].code === this.membre.fkArbre){
            this.appConfig.userConnected.arbres[i].g1.push(result.response);
          }
        }

        this.dialog.open(MessageDialogComponent, {
          data: result.error.errorDescription
        });

        

        this.router.navigate(['main/main-filleul'], { replaceUrl: true });
        //form.resetForm();
      }
    ).catch(error => {
      this.dialog.open(MessageDialogComponent, {
        data: error
      });
    })
  }

  onVerifyNumber(): void{
    console.log('Ici')
    this.membre.telephone = Util.verify(this.membre.telephone);
  }

  onSelect(value: string): void{
    this.membre.fkArbre = value
  }

  // isPasswordCorrect(): boolean{
  //   return this.membre.login.password === this.confirmPassword
  // }

  isEmailCorrect(): boolean{
    return Util.verifyEmail(this.membre.email);
  }

}
