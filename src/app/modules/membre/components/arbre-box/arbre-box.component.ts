import { Component, OnInit, Input } from '@angular/core';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';

@Component({
  selector: 'app-arbre-box',
  templateUrl: './arbre-box.component.html',
  styleUrls: ['./arbre-box.component.css']
})
export class ArbreBoxComponent implements OnInit {

  @Input() arbre: ArbreAdapter = new ArbreAdapter(); 
  constructor() { }

  ngOnInit(): void {

  }

}
