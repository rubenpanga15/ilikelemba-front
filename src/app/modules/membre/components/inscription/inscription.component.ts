import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { Util } from 'src/app/utilities/util';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  membre: MembreAdapter = new MembreAdapter();
  arbres: ArbreAdapter[];

  isChecked:boolean = false;
  confirmPassword: string;
  
  constructor(
    private service: GestionFilleulService,
    private appConfig: AppConfigService,
    private router: Router,
    private dialog: MatDialog
  ) { 
    this.membre.sexe = 'M',
    this.appConfig.getArbres().then(
      result => {
        this.arbres = null;
        this.arbres = result.response
      }
    ).catch()
  }

  ngOnInit(): void {
    this.appConfig.verifyUser();
    // if(!this.appConfig.userConnected.nom){
    //   this.router.navigate(['main']);
    // }
  }

  onSubmit(): void{
    this.service.inscription(JSON.stringify(this.membre)).then(
      result => {
        this.dialog.open(MessageDialogComponent, {
          data: result.error.errorDescription
        })
        this.router.navigate(['/main'], { replaceUrl: true })
      }
    ).catch(error => {
      this.dialog.open(MessageDialogComponent, {
        data: error
      })
    })
  }

  onSelect(value: string): void{
    this.membre.fkArbre = value
  }

  isPasswordCorrect(): boolean{
    return this.membre.login.password === this.confirmPassword
  }

  isEmailCorrect(): boolean{
    return Util.verifyEmail(this.membre.email);
  }
}
