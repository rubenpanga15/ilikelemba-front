import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { Router } from '@angular/router';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit ,OnDestroy {

  data = {
    username: '',
    password: ''
  }

  waiting: boolean = false;
  waitingSubscription: Subscription;

  constructor(
    private authentService: AuthentificationService,
    private  appConfig: AppConfigService,
    private router: Router,
    private dialogRef: MatDialogRef<LoginDialogComponent>,
    private dialog: MatDialog
  ) { }
  ngOnDestroy(): void {
    this.waitingSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.waitingSubscription = this.appConfig.waiting$.subscribe(
      (next: boolean) => this.waiting = next
    )
  }

  onSubmit():void{
    this.authentService.onAuthentification(this.data).then(
      (result: HttpDataResponse<MembreAdapter>) =>{
        this.appConfig.onConnected(result.response);
        this.appConfig.userConnected = result.response;
        this.dialogRef.close();
        this.router.navigate(['/main'], { replaceUrl: true })
      }
    ).catch(error => {
      this.dialog.open(MessageDialogComponent, {
        data: error
      })
    })
  }


}
