import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription, Subject } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { Router } from '@angular/router';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';
import { AppConfigService } from 'src/app/services/app-config.service';
import { FabAddFilleulLayoutComponent } from 'src/app/modules/shared-module/components/fab-add-filleul-layout/fab-add-filleul-layout.component';
import { AddBottomSheetComponent } from '../add-bottom-sheet/add-bottom-sheet.component';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit, OnDestroy {

  matcher: boolean = false;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => {
        this.matcher = result.matches;
        return result.matches;
      }),
      shareReplay()
    );

    fab: boolean = true;
    subscription: Subscription;
    userConnected: MembreAdapter;
    userSubscription: Subscription;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private bottomSheet: MatBottomSheet,
    private router: Router,
    private dialog: MatDialog,
    private appConfig: AppConfigService
    ) {
    }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }
  ngOnInit(): void {
    this.appConfig.verifyUser();
    // if(!this.appConfig.userConnected.nom){
    //   this.router.navigate(['main']);
    // }
    this.subscription =this.appConfig.fab$.subscribe(
      (next) => this.fab = next
    );
    this.userSubscription = this.appConfig.userConnected$.subscribe(
      (next: MembreAdapter) => {
        this.userConnected = next;
      }
    );
    this.userConnected = this.appConfig.userConnected;
    this.userConnected.nom = undefined;
    
  }

  openSheet():void{
    let bottomRef = this.bottomSheet.open(AddBottomSheetComponent);

    bottomRef.afterDismissed().subscribe(
      data =>{
        if(data){
          this.appConfig.hideFAB()
          this.router.navigate([data.link])
        }
        
      }
    )
  }

  closeDrawer(drawer: MatSidenav): void{
    if(this.matcher)
      drawer.close()
  }

  disconnect(): void{
    this.appConfig.userConnected$.next(new MembreAdapter())
    this.router.navigate(['main/']);
  }

  launchDialog(): void{
    let dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '100%'
    })
}

}
