import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { AccueilComponent } from './components/accueil/accueil.component';
import { AjoutFilleulComponent } from './components/ajout-filleul/ajout-filleul.component';
import { MainFilleulsComponent } from './components/main-filleuls/main-filleuls.component';
import { CreateBrancheComponent } from './components/create-branche/create-branche.component';
import { InscriptionComponent } from './components/inscription/inscription.component';
import { Login } from 'src/app/models/login.model';
import { LoginComponent } from '../administrateur/components/login/login.component';
import { UserConnectedGuard } from 'src/app/guards/user-connected.guard';


const routes: Routes = [
  {
    path: 'main', component: MainComponent,
    children:[
      {
        path: '', component: AccueilComponent
      },
      {
        path: 'add-filleul', component: AjoutFilleulComponent
      },
      {
        path: 'create-branche', component: CreateBrancheComponent
      },
      {
        path: 'main-filleul', component: MainFilleulsComponent
      }      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembreRoutingModule { }
