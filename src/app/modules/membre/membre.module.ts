import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MembreRoutingModule } from './membre-routing.module';
import { MainComponent } from './components/main/main.component';
import { AccueilComponent } from './components/accueil/accueil.component';
import { SharedModuleModule } from '../shared-module/shared-module.module';
import { AjoutFilleulComponent } from './components/ajout-filleul/ajout-filleul.component';
import { MainFilleulsComponent } from './components/main-filleuls/main-filleuls.component';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { AddBottomSheetComponent } from './components/add-bottom-sheet/add-bottom-sheet.component';
import { CreateBrancheComponent } from './components/create-branche/create-branche.component';
import { LoginDialogComponent } from './components/login-dialog/login-dialog.component';
import { ArbreBoxComponent } from './components/arbre-box/arbre-box.component';
import { BrancheBoxComponent } from './components/branche-box/branche-box.component';
import { InscriptionComponent } from './components/inscription/inscription.component';

@NgModule({
  declarations: [MainComponent, AccueilComponent, AjoutFilleulComponent, MainFilleulsComponent, MainNavComponent, AddBottomSheetComponent, CreateBrancheComponent, LoginDialogComponent, ArbreBoxComponent, BrancheBoxComponent, InscriptionComponent],
  imports: [
    CommonModule,
    MembreRoutingModule,
    SharedModuleModule
  ],
  entryComponents:[
    AddBottomSheetComponent, LoginDialogComponent
  ],
  exports:[
    ArbreBoxComponent,BrancheBoxComponent
  ]
})
export class MembreModule { }
