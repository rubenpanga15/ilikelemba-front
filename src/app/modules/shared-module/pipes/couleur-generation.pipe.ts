import { Pipe, PipeTransform } from '@angular/core';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';

@Pipe({
  name: 'couleurGeneration'
})
export class CouleurGenerationPipe implements PipeTransform {

  transform(value: ArbreAdapter, ...args: string[]): string {
    let arg: string = args[0];
    let couleur = 'badge-success';

    if(value){
      switch(arg){
        case 'g1':{
          if(value.g1.length >= 5)
            couleur = 'badge-danger';
          break;
        }
        case 'g2':{
          if(value.g2.length >= 25)
            couleur = 'badge-danger';
          break;
        }
        case 'g3':{
          if(value.g3.length >= 125)
            couleur = 'badge-danger';
          break;
        }
      }
    }

    return couleur;
  }

}
