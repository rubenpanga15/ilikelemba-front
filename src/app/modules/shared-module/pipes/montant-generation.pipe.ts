import { Pipe, PipeTransform } from '@angular/core';
import { Membre } from 'src/app/models/membre.model';

@Pipe({
  name: 'montantGeneration'
})
export class MontantGenerationPipe implements PipeTransform {

  transform(value: Membre[], ...args: number[]): number {
    let total = 0
    let percent = args[0]
    let montant = args[1]

    if(value){
      total += value.length * montant * percent;
    }
    return total;
  }

}
