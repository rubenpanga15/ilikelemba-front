import { Pipe, PipeTransform } from '@angular/core';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';

@Pipe({
  name: 'montantTotalArbre'
})
export class MontantTotalArbrePipe implements PipeTransform {

  transform(value: ArbreAdapter, ...args: any[]): number {
    let total: number = 0;

    if(value){
        total += value.g1.length * value.montant * value.partage.g1;
        total += value.g2.length * value.montant * value.partage.g2;
        total += value.g3.length * value.montant * value.partage.g3;
    }

    return total;
  }

}
