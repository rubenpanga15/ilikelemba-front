import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule} from '@angular/material/input'
import { MatSelectModule} from '@angular/material/select'
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { CouleurGenerationPipe } from './pipes/couleur-generation.pipe';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule } from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatDialogModule} from '@angular/material/dialog'
import { FabAddFilleulLayoutComponent } from './components/fab-add-filleul-layout/fab-add-filleul-layout.component';
import { NombreFilleulsPipe } from './pipes/nombre-filleuls.pipe';
import { StatutMembrePipe } from './pipes/statut-membre.pipe';
import { MontantGenerationPipe } from './pipes/montant-generation.pipe';
import { MontantTotalArbrePipe } from './pipes/montant-total-arbre.pipe';
import { InputDialogComponent } from './dialogs/input-dialog/input-dialog.component';
import { MessageDialogComponent } from './dialogs/message-dialog/message-dialog.component';


@NgModule({
  declarations: [CouleurGenerationPipe, FabAddFilleulLayoutComponent, NombreFilleulsPipe, StatutMembrePipe, MontantGenerationPipe, MontantTotalArbrePipe, InputDialogComponent, MessageDialogComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatBottomSheetModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatProgressBarModule,
    HttpClientModule,
    MatCardModule,
    FormsModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatBottomSheetModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatProgressBarModule,
    HttpClientModule,
    MatCardModule,
    FormsModule,
    MatIconModule,
    MatListModule,
    MatDialogModule
  ],
  exports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatBottomSheetModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatProgressBarModule,
    HttpClientModule,
    MatCardModule,
    FormsModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    CouleurGenerationPipe,
    MatDialogModule,
    FabAddFilleulLayoutComponent,
    NombreFilleulsPipe,
    StatutMembrePipe,
    MontantGenerationPipe,
    MontantTotalArbrePipe,
    InputDialogComponent,
    MessageDialogComponent,
    MatToolbarModule,
    MatButtonModule,
    MatBottomSheetModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatProgressBarModule,
    HttpClientModule,
    MatCardModule,
    FormsModule,
    MatIconModule,
    MatListModule,
    MatDialogModule
  ],
  entryComponents:[
    FabAddFilleulLayoutComponent,
    InputDialogComponent, MessageDialogComponent
  ]
})
export class SharedModuleModule { }
