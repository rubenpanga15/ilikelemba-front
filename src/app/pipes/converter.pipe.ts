import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'converter'
})
export class ConverterPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    if(!value) return '0'
    return value.toFixed(3);
  }

}
