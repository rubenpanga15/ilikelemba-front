import { Pipe, PipeTransform } from '@angular/core';
import { ParrainageAdapter } from '../models/utilities/parrainage-adapter';

@Pipe({
  name: 'nombreFilleulsActives'
})
export class NombreFilleulsActivesPipe implements PipeTransform {

  transform(value: ParrainageAdapter[], ...args: unknown[]): number {
    let nbreFilleuls = 0;
    if(value){
      let p = value.filter(e => e.statut == true);
      nbreFilleuls = p.length;
    }
    return nbreFilleuls;
  }

}
