import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'percent'
})
export class PercentPipe implements PipeTransform {

  transform(value: number, ...args: number[]): string {
    let total = 0;

    if(value){
      total = value * args[0] / 100;
    }

    return total.toFixed(3);
  }

}
