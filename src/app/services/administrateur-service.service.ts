import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Administrateur } from '../models/administrateur.model';
import { JournalAppro } from '../models/journal-appro';
import { Paiement } from '../models/paiement';
import { ParrainageAdapter } from '../models/utilities/parrainage-adapter';
import { RapportAdapter } from '../models/utilities/rapport-adapter';
import { AppConst } from '../utilities/app-const';
import { ErrorResponse } from '../utilities/error-response';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpURLs } from '../utilities/http-urls';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class AdministrateurServiceService {

  userConnected: Administrateur;
  userConnected$: Subject<Administrateur> = new Subject<Administrateur>();

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfigService
  ) { }

  connexion(data: string): Promise<HttpDataResponse<Administrateur>>{
    return new Promise<HttpDataResponse<Administrateur>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_CONNEXION_ADMIN, data).subscribe(
        (result: HttpDataResponse<Administrateur>) =>{
          console.log(result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          this.userConnected = result.response;
          this.userConnected$.next(result.response);
          resolve(result);
        },
        error => reject(AppConst.NETWORK_ERROR)
      )
    })
  }

  searchParrainage(data: string): Promise<HttpDataResponse<ParrainageAdapter[]>>{
    return new Promise<HttpDataResponse<ParrainageAdapter[]>>((resolve, reject) =>{
      this.httpClient.post(HttpURLs.URL_LIST_PARRAINAGE, data).subscribe(
        (result: HttpDataResponse<ParrainageAdapter[]>) =>{
          console.log(result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => reject(AppConst.NETWORK_ERROR)
      )
    })
  }

  rapport(data: string): Promise<HttpDataResponse<RapportAdapter>>{
    return new Promise<HttpDataResponse<RapportAdapter>>((resolve, reject) =>{
      this.httpClient.post(HttpURLs.URL_LIST_PARRAINAGE_DATE, data).subscribe(
        (result: HttpDataResponse<RapportAdapter>) =>{
          console.log(result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => reject(AppConst.NETWORK_ERROR)
      )
    })
  }

  activer(data: string): Promise<HttpDataResponse<boolean>>{
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) =>{
      this.httpClient.post(HttpURLs.URL_ACTIVER_MEMBRE, data).subscribe(
        (result: HttpDataResponse<boolean>) =>{
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => alert(AppConst.NETWORK_ERROR)
      )
    })
  }

  getListPaiement(data: string): Promise<HttpDataResponse<Paiement[]>>{
    this.appConfig.onStartWaiting('Récupration de la liste des paiements...');
    return new Promise<HttpDataResponse<Paiement[]>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_LIST_PAIEMENT_ADMIN, data).subscribe(
        (result: HttpDataResponse<Paiement[]>) => {
          console.log(result);
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  effectuerPaiement(data: string): Promise<HttpDataResponse<boolean>>{
    this.appConfig.onStartWaiting();
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_PAIEMENT, data).subscribe(
        (result: HttpDataResponse<boolean>) => {
          console.log(result);
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  annulerPaiement(data: string): Promise<HttpDataResponse<boolean>>{
    this.appConfig.onStartWaiting();
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_ANNULER_PAIEMENT, data).subscribe(
        (result: HttpDataResponse<boolean>) => {
          console.log(result);
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  getDashboard(data: string): Promise<HttpDataResponse<RapportAdapter>>{
    this.appConfig.onStartWaiting('Récuperation des données en cours');
    return new Promise<HttpDataResponse<RapportAdapter>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_DASHBOARD, data).subscribe(
        (result: HttpDataResponse<RapportAdapter>) => {
          console.log(result);
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    });
  }

  getListJournalForAgent(data: string): Promise<HttpDataResponse<JournalAppro[]>>{
    this.appConfig.onStartWaiting('Récuperation des données en cours');
    return new Promise<HttpDataResponse<JournalAppro[]>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_JOURNALAPPRO_FOR_PROVENANCE_AGENT, data).subscribe(
        (result: HttpDataResponse<JournalAppro[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    });
  }

  approvisionnerManager(data: string): Promise<HttpDataResponse<boolean>>{
    this.appConfig.onStartWaiting();
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_ACCREDITER_COMPTE, data).subscribe(
        (result: HttpDataResponse<boolean>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  approvisionnerAgence(data: string): Promise<HttpDataResponse<boolean>>{
    this.appConfig.onStartWaiting();
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_ACCREDITER_AGENCE, data).subscribe(
        (result: HttpDataResponse<boolean>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  getAllAdmins(): Promise<HttpDataResponse<Administrateur[]>>{
    this.appConfig.onStartWaiting();
    return new Promise<HttpDataResponse<Administrateur[]>>((resolve, reject) => {
      this.httpClient.get(HttpURLs.URL_ALL_ADMINS).subscribe(
        (result: HttpDataResponse<Administrateur[]>) => {
          console.log(result);
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }
}
