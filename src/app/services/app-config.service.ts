import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MembreAdapter } from '../models/utilities/membre-adapter';
import { Devise } from '../models/devise';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpClient } from '@angular/common/http';
import { ErrorResponse } from '../utilities/error-response';
import { HttpURLs } from '../utilities/http-urls';
import { AppConst } from '../utilities/app-const';
import { ArbreAdapter } from '../models/utilities/arbre-adapter';
import { UserConnectedGuard } from '../guards/user-connected.guard';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { WaitingDialogComponent } from '../dialogs/waiting-dialog/waiting-dialog.component';
import { Config } from 'src/app/models/config'

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  waiting$: Subject<boolean> = new Subject<boolean>();
  fab$: Subject<boolean> = new Subject<boolean>();
  isConnected$: Subject<boolean> = new Subject<boolean>();
  userConnected$: Subject<MembreAdapter> = new Subject<MembreAdapter>();

  userConnected: MembreAdapter;

  devises: Devise[];

  private dialogRef: MatDialogRef<WaitingDialogComponent>;
  static USER_KEY: string = "ilikelemba-user";
  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private dialog: MatDialog
  ) {
   }


  onStartWaiting(message?: string): void{
    this.waiting$.next(true);

    this.dialogRef = this.dialog.open(WaitingDialogComponent, {
      disableClose: true,
      data: message
    });
  }

  onStopWaiting(): void{
    this.waiting$.next(false);
    this.dialogRef.close();
  }

  showFAB():void{
    this.fab$.next(true);
  }
  hideFAB():void{
    this.fab$.next(false);
  }
  onConnected(membre: MembreAdapter): void{
    sessionStorage.setItem(AppConfigService.USER_KEY, JSON.stringify(membre));
    this.userConnected = new MembreAdapter;
    this.userConnected = membre;
    this.userConnected$.next(membre);
    this.isConnected$.next(true);
  }
  onDisconnected():void{
    sessionStorage.setItem(AppConfigService.USER_KEY, undefined);
    this.isConnected$.next(false);
    this.userConnected = undefined;
    this.userConnected$.next(undefined)
  }

  getDevise(): Promise<HttpDataResponse<Devise[]>>{
    return new Promise<HttpDataResponse<Devise[]>>((resolve, reject) =>{
      let httpDataResponse: HttpDataResponse<Devise[]> = new HttpDataResponse<Devise[]>();
      if(this.devises){
        httpDataResponse.error = new ErrorResponse();
        httpDataResponse.error.errorCode = ErrorResponse.OK;
        httpDataResponse.response = this.devises;
        resolve(httpDataResponse);
      }
      this.httpClient.get(HttpURLs.URL_LIST_DEVISES).subscribe(
        (result: HttpDataResponse<Devise[]>) =>{
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => reject(AppConst.NETWORK_ERROR)
      )
    })
  }

  getArbres(): Promise<HttpDataResponse<ArbreAdapter[]>>{
    return new Promise<HttpDataResponse<ArbreAdapter[]>>((resolve, reject) =>{
      this.httpClient.get(HttpURLs.URL_LIST_ARBRES).subscribe(
        (result: HttpDataResponse<ArbreAdapter[]>) =>{
          console.log(result)
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => reject(AppConst.NETWORK_ERROR)
      )
    })
  }

  verifyUser(): void{
    const userJson:string = localStorage.getItem(AppConfigService.USER_KEY);
    console.log(userJson)
      if(userJson){
        this.userConnected = JSON.parse(userJson);
      }
  }

  getConfig(data: string): Promise<HttpDataResponse<Config>>{
    this.onStartWaiting('Récuperation de donéées en cours....');
    return new Promise<HttpDataResponse<Config>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_GET_CONFIG, data).subscribe(
        (result: HttpDataResponse<Config>) =>{
          this.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }
  
}
