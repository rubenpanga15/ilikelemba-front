import { Injectable } from '@angular/core';
import { Membre } from '../models/membre.model';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpClient } from '@angular/common/http';
import { HttpURLs } from '../utilities/http-urls';
import { ErrorResponse } from '../utilities/error-response';
import { AppConst } from '../utilities/app-const';
import { AppConfigService } from './app-config.service';
import { MembreAdapter } from '../models/utilities/membre-adapter';
import { Arbre } from '../models/arbre.model';
import { ArbreAdapter } from '../models/utilities/arbre-adapter';
import { Paiement } from '../models/paiement';
import { ParrainageAdapter } from '../models/utilities/parrainage-adapter';

@Injectable({
  providedIn: 'root'
})
export class GestionFilleulService {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfigService
  ) { }


  addFilleul(membre: MembreAdapter): Promise<HttpDataResponse<Membre>>{
    return new Promise<HttpDataResponse<Membre>>((resolve, reject) =>{
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpURLs.URL_ADD_FILLEUL, JSON.stringify(membre)).subscribe(
        (result: HttpDataResponse<Membre>) =>{
          this.appConfig.onStopWaiting();
          console.log(result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          reject(AppConst.NETWORK_ERROR);
          this.appConfig.onStopWaiting();
        },
        () => this.appConfig.onStopWaiting()
      )
    })
  }

  createArbre(arbre: Arbre): Promise<HttpDataResponse<ArbreAdapter>>{
    return new Promise<HttpDataResponse<ArbreAdapter>>((resolve, reject) =>{
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpURLs.URL_CREATE_ARBRE, JSON.stringify(arbre)).subscribe(
        (result: HttpDataResponse<ArbreAdapter>) =>{
          console.log(result);
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  activer(data: string): Promise<HttpDataResponse<boolean>>{
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) =>{
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpURLs.URL_ACTIVER_MEMBRE, data).subscribe(
        (result: HttpDataResponse<boolean>) =>{
          console.log(result);
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  inscription(data: string): Promise<HttpDataResponse<Membre>>{
    return new Promise<HttpDataResponse<Membre>>((resolve, reject) =>{
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpURLs.URL_INSCRPTION, data).subscribe(
        (result: HttpDataResponse<Membre>) =>{
          this.appConfig.onStopWaiting();
          console.log(result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          reject(AppConst.NETWORK_ERROR);
          this.appConfig.onStopWaiting();
        },
        () => this.appConfig.onStopWaiting()
      )
    })
  }

  effectuerPaiement(data: string): Promise<HttpDataResponse<number>>{
    this.appConfig.onStartWaiting();
    return new Promise<HttpDataResponse<number>>((resolve, reject) =>{
      this.httpClient.post(HttpURLs.URL_RETRAIT, data)
      .subscribe(
        (result: HttpDataResponse<number>) => {
          this.appConfig.onStopWaiting();
          console.log(result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    })
  }

  getListPaiements(data: number): Promise<HttpDataResponse<Paiement[]>>{
    this.appConfig.onStartWaiting('Récuperation de la liste des paiements');
    return new Promise<HttpDataResponse<Paiement[]>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_LIST_PAIEMENT_MEMBRE, JSON.stringify({fkMembre: data}))
      .subscribe(
        (result: HttpDataResponse<Paiement[]>) => {
          this.appConfig.onStopWaiting();
          console.log(result);
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  annulerPaiement(data: string): Promise<HttpDataResponse<boolean>>{
    this.appConfig.onStartWaiting();
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_ANNULER_PAIEMENT, data).subscribe(
        (result: HttpDataResponse<boolean>) => {
          console.log(result);
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  effectuerPaiementManager(data: string): Promise<HttpDataResponse<boolean>>{
    this.appConfig.onStartWaiting();
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_EFFECTUER_PAIEMENT_MANAGER, data).subscribe(
        (result: HttpDataResponse<boolean>) => {
          console.log(result);
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  getMembre(data: string): Promise<HttpDataResponse<MembreAdapter>>{
    this.appConfig.onStartWaiting();
    return new Promise<HttpDataResponse<MembreAdapter>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_GET_MEMBRE, data).subscribe(
        (result: HttpDataResponse<MembreAdapter>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  accrediterCompte(data: string): Promise<HttpDataResponse<boolean>>{
    this.appConfig.onStartWaiting();
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) => {
      this.httpClient.post(HttpURLs.URL_ACCREDITER_COMPTE, data).subscribe(
        (result: HttpDataResponse<boolean>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  searchParrainage(data: string): Promise<HttpDataResponse<ParrainageAdapter[]>>{
    return new Promise<HttpDataResponse<ParrainageAdapter[]>>((resolve, reject) =>{
      this.appConfig.onStartWaiting('Recherche en cours');
      this.httpClient.post(HttpURLs.URL_LIST_PARRAINAGE, data).subscribe(
        (result: HttpDataResponse<ParrainageAdapter[]>) =>{
          console.log(result);
          this.appConfig.onStopWaiting()
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    })
  }

  getMesActivations(data: string): Promise<HttpDataResponse<ParrainageAdapter[]>>{
    return new Promise<HttpDataResponse<ParrainageAdapter[]>>((resolve, reject) =>{
      this.appConfig.onStartWaiting('Recuperations en cours');
      this.httpClient.post(HttpURLs.URL_MES_ACTIVATIONS, data).subscribe(
        (result: HttpDataResponse<ParrainageAdapter[]>) =>{
          console.log(result);
          this.appConfig.onStopWaiting()
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    })
  }

  activerFilleul(data: string): Promise<HttpDataResponse<boolean>>{
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) =>{
      this.appConfig.onStartWaiting('Activation en cours');
      this.httpClient.post(HttpURLs.URL_ACTIVER_FILLEUL_MANAGER, data).subscribe(
        (result: HttpDataResponse<boolean>) =>{
          this.appConfig.onStopWaiting()
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    });
  }

  getListPaiementsManager(data: string): Promise<HttpDataResponse<Paiement[]>>{
    return new Promise<HttpDataResponse<Paiement[]>>((resolve, reject) =>{
      this.appConfig.onStartWaiting('Recuperation de la liste en cours');
      this.httpClient.post(HttpURLs.URL_MES_PAIEMENTS_MANAGER, data).subscribe(
        (result: HttpDataResponse<Paiement[]>) =>{
          this.appConfig.onStopWaiting()
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    });
  }

  getAllmembre(): Promise<HttpDataResponse<Membre[]>>{
    return new Promise<HttpDataResponse<Membre[]>>((resolve, reject) =>{
      this.httpClient.post(HttpURLs.URL_ALL_MEMBRE, '').subscribe(
        (result: HttpDataResponse<Membre[]>) =>{
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          reject(AppConst.NETWORK_ERROR)
        }
      )
    });
  }

  configLogin(data: string): Promise<HttpDataResponse<boolean>>{
    return new Promise<HttpDataResponse<boolean>>((resolve, reject) =>{
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpURLs.URL_CONFIG, data).subscribe(
        (result: HttpDataResponse<boolean>) =>{
          this.appConfig.onStopWaiting()
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting()
          reject(AppConst.NETWORK_ERROR)
        }
      )
    });
  }

  
}
