

export class HttpURLs {

    private static SCHEMAS: string = "http";
    private static IP: string = "95.142.174.164";
    //private static IP: string = "localhost";
    //private static IP: string = "192.168.43.28";

    private static PORT: string = "8080";
    //private static PORT: string = "8089";
    private static PORT_SOCKET: string = '4000';

    private static url: string = 'www.likelemba-intelligent.com/inscription';
    //private static url:string = 'localhost:4200/inscription'

    private static getBase(): string{
        //return HttpURLs.SCHEMAS + "://" + HttpURLs.IP + ":" + HttpURLs.PORT + "/iLikelembaWeb/"
        return HttpURLs.SCHEMAS + "://" + HttpURLs.IP + ":" + HttpURLs.PORT + "/ilikelemba/"
    }

    static getUrlInscription(): string{
        return HttpURLs.url;
    }

    static getBaseSocket(): string{
        return HttpURLs.SCHEMAS + "://" + HttpURLs.IP + ":" + HttpURLs.PORT_SOCKET + "";
    }

    static URL_ADD_FILLEUL = HttpURLs.getBase() + 'api/add-filleul';
    static URL_LOGIN = HttpURLs.getBase() + 'api/connexion';
    static URL_LIST_DEVISES: string = HttpURLs.getBase() + 'api/list-devises';
    static URL_CREATE_ARBRE: string = HttpURLs.getBase() + 'api/create-arbre';
    static URL_ACTIVER_MEMBRE: string = HttpURLs.getBase() + 'api/activer-filleul';
    static URL_CONNEXION_ADMIN: string = HttpURLs.getBase()+ 'api/admin-connexion';
    static URL_LIST_PARRAINAGE: string = HttpURLs.getBase() + 'api/list-parrainage';
    static URL_LIST_PARRAINAGE_DATE: string = HttpURLs.getBase() + 'api/rapport-activites';
    static URL_LIST_ARBRES: string = HttpURLs.getBase() + 'api/list-arbres';
    static URL_INSCRPTION: string = HttpURLs.getBase() + 'api/inscription';
    static URL_PAIEMENT: string = HttpURLs.getBase() + 'api/paiement';
    static URL_ANNULER_PAIEMENT: string = HttpURLs.getBase() + 'api/annuler-paiement';
    static URL_RETRAIT: string = HttpURLs.getBase() + 'api/retrait';
    static URL_LIST_PAIEMENT_ADMIN: string = HttpURLs.getBase() + 'api/list-paiement-admin';
    static URL_LIST_PAIEMENT_MEMBRE: string = HttpURLs.getBase() + 'api/list-paiement-membre';
    static URL_GET_MEMBRE: string = HttpURLs.getBase() + 'api/get-membre';
    static URL_ACCREDITER_COMPTE: string = HttpURLs.getBase() + 'api/approvisionner-compte';
    static URL_ACCREDITER_AGENCE: string = HttpURLs.getBase() + 'api/appro-agence';
    static URL_DASHBOARD: string = HttpURLs.getBase() + 'api/dashboard';
    static URL_JOURNALAPPRO_FOR_PROVENANCE_AGENT: string = HttpURLs.getBase() + 'api/journal-provenance-agent';
    static URL_ACTIVER_FILLEUL_MANAGER: string =  HttpURLs.getBase() + 'api/activer-manager';
    static URL_MES_ACTIVATIONS: string = HttpURLs.getBase() + 'api/mesactivations';
    static URL_MES_PAIEMENTS_MANAGER: string = HttpURLs.getBase() + 'api/paiements-manager';
    static URL_EFFECTUER_PAIEMENT_MANAGER: string = HttpURLs.getBase() + 'api/effectuer-paiementmanager'
    static URL_GET_CONFIG: string = HttpURLs.getBase() + 'api/get-frais';
    static URL_ALL_MEMBRE: string = HttpURLs.getBase() + 'api/list-all-membre';
    static URL_CONFIG: string = HttpURLs.getBase() + 'api/config-membre';
    static URL_ALL_ADMINS: string = HttpURLs.getBase() + 'api/list-admins';
}
