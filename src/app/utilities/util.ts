

export class Util {

    static verify(str: string): string{
        let data: string = '';
        
        if(str.length > 0){
            for(let i = 0; i < str.length; i++){
                if(!isNaN(parseInt(str[i])))
                    data += str[i];
            }
        }

        return data;
    }

    static verifyEmail(str: string): boolean{
        if(!str || str.length === 0)
            return true;
        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(str);
    }
}
