import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { ErrorDialogComponent } from '../dialogs/error-dialog/error-dialog.component';
import { InformationDialogComponent } from '../dialogs/information-dialog/information-dialog.component';
import { SuccesDialogComponent } from '../dialogs/succes-dialog/succes-dialog.component';
import { WaitingDialogComponent } from '../dialogs/waiting-dialog/waiting-dialog.component';

export class Utility {

    static openInfoDialog(dialog: MatDialog, message: string): void{
        dialog.open(InformationDialogComponent, {data: message})
    }

    static openErrorDialog(dialog: MatDialog, message: string): void{
        dialog.open(ErrorDialogComponent, {data: message})
    }

    static openConfirmDialog(dialog: MatDialog, message: string): MatDialogRef<ConfirmDialogComponent>{
        return dialog.open(ConfirmDialogComponent, {data: message, disableClose: true});
    }

    static openSuccessDialog(dialog: MatDialog, message: string): void{
        dialog.open(SuccesDialogComponent, {data: message})
    }

    static verify(str: string): string{
        let data: string = '';
        
        if(str.length > 0){
            for(let i = 0; i < str.length; i++){
                if(!isNaN(parseInt(str[i])))
                    data += str[i];
            }
        }

        return data;
    }

    static verifyEmail(str: string): boolean{
        if(!str || str.length === 0)
            return true;
        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(str);
    }
}
