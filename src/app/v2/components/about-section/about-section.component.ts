import { Component, OnInit } from '@angular/core';
import { Fade } from 'src/app/animations/fade';

@Component({
  selector: 'app-about-section',
  templateUrl: './about-section.component.html',
  styleUrls: ['./about-section.component.css'],
  animations: [Fade]
})
export class AboutSectionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
