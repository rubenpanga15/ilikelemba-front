import { Component, OnInit } from '@angular/core';
import { Fade } from 'src/app/animations/fade';
import { AppConfigService } from 'src/app/services/app-config.service';
import * as AOS from 'aos'
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';

@Component({
  selector: 'app-accueil-section',
  templateUrl: './accueil-section.component.html',
  styleUrls: ['./accueil-section.component.css'],
  animations: [Fade]
})
export class AccueilSectionComponent implements OnInit {

  isConnected : boolean = false;

  nombre: number;

  constructor(
    private appConfig: AppConfigService,
    private service: GestionFilleulService
  ) { }

  ngOnInit(): void {
     AOS.init();
    if(this.appConfig.userConnected)
      this.isConnected = true;
    this.appConfig.isConnected$.subscribe(
      next => this.isConnected = next
    )

    this.service.getAllmembre().then(res => this.nombre = res.response.length);
  }

}
