import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Config } from 'src/app/models/config';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';
import { InputDialogComponent } from 'src/app/modules/shared-module/dialogs/input-dialog/input-dialog.component';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { AppConst } from 'src/app/utilities/app-const';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-approvisionnement',
  templateUrl: './approvisionnement.component.html',
  styleUrls: ['./approvisionnement.component.css']
})
export class ApprovisionnementComponent implements OnInit {

  telephone:string = '';
  membre: MembreAdapter;
  montantcdf: number = 0;
  montantusd: number = 0;
  config: Config

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private service: GestionFilleulService,
    private adminService: AdministrateurServiceService
  ) { }

  ngOnInit(): void {
    this.appConfig.getConfig(JSON.stringify({code: AppConst.BENEFICE_MANAGER})).then(
      res => this.config = res.response
    ).catch( error => Utility.openInfoDialog(this.dialog, error))
  }

  onSearch(): void{
    this.membre = undefined;
    this.service.getMembre(JSON.stringify({telephone: this.telephone}))
    .then(
      res => this.membre = res.response
    ).catch( error => Utility.openInfoDialog(this.dialog, error))
  }

  onSubmit(): void{
    let dialogRef = this.dialog.open<InputDialogComponent>(InputDialogComponent, 
      {
        data: 'Veuillez inserer votre mot de passe'
      });
      dialogRef.afterClosed().subscribe(
        (password: string) =>{
          if(this.adminService.userConnected.password === password){
            this.adminService.approvisionnerManager(JSON.stringify(
              {
                fkDestinateur: this.membre.id,
                montantcdf: this.montantcdf,
                montantusd: this.montantusd,
                fkProvenance: this.adminService.userConnected.id

              }
            )).then(
              res => {
                this.montantcdf = 0;
                this.montantusd = 0;
                Utility.openSuccessDialog(this.dialog, res.error.errorDescription)
              }
            ).catch(
              error => Utility.openInfoDialog(this.dialog, error)
            )
          }else{
            this.dialog.open(MessageDialogComponent, {
              data: 'Le mot de passe fournit est incorrect'
            })
          }
        }
      )
  }

   

}
