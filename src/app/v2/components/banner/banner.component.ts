import { Component, OnInit } from '@angular/core';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  isConnected: boolean = false;

  constructor(
    private appConfig: AppConfigService
  ) { 
    if(appConfig.userConnected)
      this.isConnected = true;
  }

  ngOnInit(): void {
    this.appConfig.isConnected$.subscribe(
      next => this.isConnected = next
    )
  }

}
