import { Component, Input, OnInit } from '@angular/core';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.css']
})
export class CompteComponent implements OnInit {

  @Input() membre: MembreAdapter

  constructor() { }

  ngOnInit(): void {
  }

}
