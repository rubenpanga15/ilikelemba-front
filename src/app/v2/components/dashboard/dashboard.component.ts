import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParrainageAdapter } from 'src/app/models/utilities/parrainage-adapter';
import { RapportAdapter } from 'src/app/models/utilities/rapport-adapter';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(
    public appConfig: AppConfigService,
    public service: AdministrateurServiceService,
    private dialog: MatDialog
  ) { }

  rapport: RapportAdapter;

  data = {
    debut: '',
    fin: ''
  }

  ngOnInit(): void {
  }

  onSubmit(): void{
    this.service.getDashboard(JSON.stringify(this.data))
    .then(
      res => this.rapport = res.response
    ).catch( error => Utility.openInfoDialog(this.dialog, error))
  }

  calculTotalCDF(): number{
    let total = 0;

    if(this.rapport && this.rapport.parrainages && this.rapport.parrainages.length > 0){
      total = this.rapport.parrainages.filter(
        e => e.dateActivation && e.fkArbre.devise.code == 'CDF'
      ).reduce<number>((previous: number, currentValue: ParrainageAdapter) => previous += currentValue.fkArbre.montant, 0)
    }

    return total;
  }

  totalActive(): number{
    let total: any[] = [];

    if(this.rapport && this.rapport.parrainages && this.rapport.parrainages.length > 0){
      total = this.rapport.parrainages.filter(
        e => e.dateActivation
      );
    }

    return total.length;
  }

  totalNonActive(): number{
    let total: any[] = [];

    if(this.rapport && this.rapport.parrainages && this.rapport.parrainages.length > 0){
      total = this.rapport.parrainages.filter(
        e => !e.dateActivation
      );
    }

    return total.length;
  }

  calculTotalUSD(): number{
    let total = 0;

    if(this.rapport && this.rapport.parrainages && this.rapport.parrainages.length > 0){
      total = this.rapport.parrainages.filter(
        e => e.dateActivation && e.fkArbre.devise.code == 'USD'
      ).reduce<number>((previous: number, currentValue: ParrainageAdapter) => previous += currentValue.fkArbre.montant, 0)
    }

    return total;
  }

}
