import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { AppConfigService } from 'src/app/services/app-config.service';
import { Fade } from 'src/app/animations/fade';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations:[Fade]
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @ViewChild('namedElement', {static: false}) namedElement: ElementRef;

  constructor(
    public appConfig: AppConfigService
  ) { }
  ngAfterViewInit(): void {
    
  }

  isConnected: boolean =false;

  userConnected: MembreAdapter

  ngOnInit(): void {
    this.appConfig.isConnected$.subscribe(
      (next: boolean) => this.isConnected = next
    );

    this.appConfig.userConnected$.subscribe(
      (next) => this.userConnected = next
    )

    if(this.appConfig.userConnected)
      this.isConnected = true;
    this.userConnected = this.appConfig.userConnected;
  }

  hide():void{
    this.namedElement.nativeElement.click();
  }

  disconnect(){
    this.appConfig.onDisconnected();
  }

}
