import { Component, OnInit } from '@angular/core';
import { Fade } from 'src/app/animations/fade';
import { Subscription } from 'rxjs';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from 'src/app/modules/membre/components/login-dialog/login-dialog.component';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { MembreAdapter } from 'src/app/models/utilities/membre-adapter';
import { MessageDialogComponent } from 'src/app/modules/shared-module/dialogs/message-dialog/message-dialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [Fade]
})
export class LoginComponent implements OnInit {

  data = {
    username: '',
    password: ''
  }

  waiting: boolean = false;
  waitingSubscription: Subscription;

  constructor(
    private authentService: AuthentificationService,
    private  appConfig: AppConfigService,
    private router: Router,
    private dialog: MatDialog
  ) { }
  ngOnDestroy(): void {
    this.waitingSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.waitingSubscription = this.appConfig.waiting$.subscribe(
      (next: boolean) => this.waiting = next
    )
  }

  onSubmit():void{
    this.authentService.onAuthentification(this.data).then(
      (result: HttpDataResponse<MembreAdapter>) =>{
        this.appConfig.onConnected(result.response);
        this.router.navigate(['/myaccount'], { replaceUrl: true })
      }
    ).catch(error => {
      this.dialog.open(MessageDialogComponent, {
        data: error
      })
    })
  }

}
