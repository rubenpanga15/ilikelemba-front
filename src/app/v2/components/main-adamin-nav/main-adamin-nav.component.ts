import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Administrateur } from 'src/app/models/administrateur.model';
import { AddBottomSheetComponent } from 'src/app/modules/membre/components/add-bottom-sheet/add-bottom-sheet.component';
import { LoginDialogComponent } from 'src/app/modules/membre/components/login-dialog/login-dialog.component';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-main-adamin-nav',
  templateUrl: './main-adamin-nav.component.html',
  styleUrls: ['./main-adamin-nav.component.css']
})
export class MainAdaminNavComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => {
        return result.matches;
      }),
      shareReplay()
    );

    fab: boolean = true;
    subscription: Subscription;
    userConnected: Administrateur = new Administrateur();
    userSubscription: Subscription;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private bottomSheet: MatBottomSheet,
    private router: Router,
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    public service: AdministrateurServiceService
    ) {
    }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }
  ngOnInit(): void {
    if(!this.service.userConnected){
      this.router.navigate(['admin/']);
    }
    this.subscription =this.appConfig.fab$.subscribe(
      (next) => this.fab = next
    );
    this.userSubscription = this.service.userConnected$.subscribe(
      (next: Administrateur) => {
        this.userConnected = next;
      }
    );
    this.userConnected = this.service.userConnected;
    
  }

  openSheet():void{
    let bottomRef = this.bottomSheet.open(AddBottomSheetComponent);

    bottomRef.afterDismissed().subscribe(
      data =>{
        if(data){
          this.appConfig.hideFAB()
          this.router.navigate([data.link])
        }
        
      }
    )
  }

  disconnect(): void{
    this.router.navigate(['admin/']);
  }

  launchDialog(): void{
    let dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '80%',
      height: '80%'
    })
}


}
