import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParrainageAdapter } from 'src/app/models/utilities/parrainage-adapter';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-mes-activations',
  templateUrl: './mes-activations.component.html',
  styleUrls: ['./mes-activations.component.css']
})
export class MesActivationsComponent implements OnInit {

  parrainages: ParrainageAdapter[] = []
  motclef: string = '';
  date: any = new Date();

  constructor(
    private service: GestionFilleulService,
    private dialog: MatDialog,
    private appConfig: AppConfigService
  ) { }

  ngOnInit(): void {
    this.searchParrainages();
  }

  filterData(): ParrainageAdapter[]{
    return this.parrainages.filter(e => (e.membre.nom.toLowerCase().includes(this.motclef.toLowerCase()) || e.membre.postnom.toLowerCase().includes(this.motclef.toLowerCase()) || e.membre.prenom.toLowerCase().includes(this.motclef.toLowerCase()) ||  e.membre.telephone.toLowerCase().includes(this.motclef.toLowerCase())) && e.statut)
  }

  searchParrainages(): void{
    this.service.getMesActivations(JSON.stringify({id: this.appConfig.userConnected.id, date: this.date})).then(
      res => this.parrainages = res.response
    ).catch(error => Utility.openInfoDialog(this.dialog, error))
  }

}
