import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Paiement } from 'src/app/models/paiement';
import { InputDialogComponent } from 'src/app/modules/shared-module/dialogs/input-dialog/input-dialog.component';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-mes-paiements',
  templateUrl: './mes-paiements.component.html',
  styleUrls: ['./mes-paiements.component.css']
})
export class MesPaiementsComponent implements OnInit {

  paiements: Paiement[];
  date: any = new Date();

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private service: GestionFilleulService,
  ) { }

  ngOnInit(): void {
    this.update()
  }

  getPaiements(): void{

  }

  annuler(value: Paiement): void{
    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûr de vouloir annuler ce paiements?')
    .afterClosed().
    subscribe(
      r => {
        if(r === 'OK'){
          this.service.annulerPaiement(JSON.stringify(value))
          .then(
            res => {
              Utility.openSuccessDialog(this.dialog, res.error.errorDescription)
              this.update()
            }
          )
        }
      }
    )
  }

  update(): void{
    this.service.getListPaiementsManager(JSON.stringify({id: this.appConfig.userConnected.id, date: this.date})).then(
      res => this.paiements = res.response
    ).catch(
      error => Utility.openErrorDialog(this.dialog, error)
    )
  }

  valider(paiement: Paiement):  void{
    let dial = this.dialog.open<InputDialogComponent>(InputDialogComponent, 
      {
        data: 'Veuillez inserer votre mot de passe'
      });
      dial.afterClosed().subscribe(
        (password: string) =>{
          if(this.appConfig.userConnected.login.password === password){
            this.service.effectuerPaiementManager(JSON.stringify(paiement)).then(
              res => {
                paiement.statut = true;
                paiement.etat = 'PAYE';
                Utility.openSuccessDialog(this.dialog, 'Paiement effectuer avec succès');
              }
            ).catch(
              error => Utility.openInfoDialog(this.dialog, error)
            )
          }else{
            Utility.openInfoDialog(this.dialog, 'Le mot de passe fournit est incorrect');
          }
        }
      )
  }



}
