import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConsultPaiementDialogComponent } from 'src/app/dialogs/consult-paiement-dialog/consult-paiement-dialog.component';
import { Paiement } from 'src/app/models/paiement';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-paiement-admin',
  templateUrl: './paiement-admin.component.html',
  styleUrls: ['./paiement-admin.component.css']
})
export class PaiementAdminComponent implements OnInit {

  data = {
    debut: undefined,
    fin: undefined,
    fkAdmin: undefined
  }

  paiements: Paiement[];

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private service: AdministrateurServiceService
  ) { 

  }

  ngOnInit(): void {
    this.data.fkAdmin = this.service.userConnected.code;
  }

  onSubmit(): void{
    this.updateData();
  }

  consult(paiement: Paiement): void{
    this.dialog.open(ConsultPaiementDialogComponent, {
      data: paiement,
      disableClose: true
    }).afterClosed().subscribe(
      res => {
        this.updateData()
      }
    )
  }

  updateData(): void{
    this.service.getListPaiement(JSON.stringify(this.data))
    .then(
      result => this.paiements = result.response
    ).catch(
      error => Utility.openErrorDialog(this.dialog, error)
    )
  }

}
