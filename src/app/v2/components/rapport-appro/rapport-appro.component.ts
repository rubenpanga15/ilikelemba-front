import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JournalAppro } from 'src/app/models/journal-appro';
import { ParrainageAdapter } from 'src/app/models/utilities/parrainage-adapter';
import { ConsultParrainageDialogComponent } from 'src/app/modules/administrateur/components/consult-parrainage-dialog/consult-parrainage-dialog.component';
import { AdministrateurServiceService } from 'src/app/services/administrateur-service.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-rapport-appro',
  templateUrl: './rapport-appro.component.html',
  styleUrls: ['./rapport-appro.component.css']
})
export class RapportApproComponent implements OnInit {

  subscription: Subscription

  constructor(
    public service: AdministrateurServiceService,
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private router: Router
  ) { 
    
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  data: any = {
    motclef: '',
    debut: '',
    fin: '',
    type: 'agence'
  }

  ngOnInit(): void {
    if(!this.service.userConnected){
      this.router.navigate(['admin/']);
    }
   this.subscription= this.appConfig.waiting$.subscribe(
      (result: boolean) =>{
        this.waiting = result;
      }
    )
  }

  journals: JournalAppro[] = []
  waiting: boolean = false;

  onSubmit(): void{
    if(this.service.userConnected.role == 'agent')
      this.data.fk = this.service.userConnected.id
    this.journals = []
    this.service.getListJournalForAgent(JSON.stringify(this.data)).then(
      (result: HttpDataResponse<JournalAppro[]>) => {
        this.journals = result.response;
      }
    ).catch(error => Utility.openInfoDialog(this.dialog, error))
  }

  filterData(): JournalAppro[]{
    if(!this.journals)
      return [];
    if(this.data.motclef.length <= 0)
      return this.journals;

    return this.journals.filter(
      e => (e.destinateur && e.destinateur.nom.toLowerCase().includes(this.data.motclef.toLowerCase())) ||
      (e.destinateur && e.destinateur.postnom.toLowerCase().includes(this.data.motclef.toLowerCase())) ||
      (e.destinateur && e.destinateur.prenom.toLowerCase().includes(this.data.motclef.toLowerCase())) ||
      (e.agentDestinateur && e.agentDestinateur.noms.toLowerCase().includes(this.data.motclef.toLowerCase())) ||
      (e.agentDestinateur && e.agentDestinateur.code.toLowerCase().includes(this.data.motclef.toLowerCase())) ||
      (e.agent && e.agent.code.toLowerCase().includes(this.data.motclef.toLowerCase())) ||
      (e.agent && e.agent.noms.toLowerCase().includes(this.data.motclef.toLowerCase()))
    )
    
  }

  totalCDF(): number{
    let total = 0;

    if(this.journals){
      total = this.journals.reduce<number>((previous, current) => previous += current.montantcdf, 0)
    }

    return total;
  }

  totalUSD(): number{
    let total = 0;

    if(this.journals){
      total = this.journals.reduce<number>((previous, current) => previous += current.montantusd, 0)
    }

    return total;
  }

  

}
