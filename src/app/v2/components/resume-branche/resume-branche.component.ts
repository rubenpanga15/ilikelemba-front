import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ArbreAdapter } from 'src/app/models/utilities/arbre-adapter';
import { Membre } from 'src/app/models/membre.model';
import { HttpURLs } from 'src/app/utilities/http-urls';
import { ParrainageAdapter } from 'src/app/models/utilities/parrainage-adapter';

@Component({
  selector: 'app-resume-branche',
  templateUrl: './resume-branche.component.html',
  styleUrls: ['./resume-branche.component.css']
})
export class ResumeBrancheComponent implements OnInit {

  @Input() arbre: ArbreAdapter

  constructor() { }

  url:string = HttpURLs.getUrlInscription();

  g2: Membre[] = []
  g3: Membre[] = []

  p2: ParrainageAdapter[] = [];
  p3: ParrainageAdapter[] = [];

  ngOnChanges(changes: SimpleChanges): void {
    this.g2 = [];
    this.g3 = [];
    this.p2 = [];
    this.p3 = [];
  }

  ngOnInit(): void {
    this.g2 = this.arbre.g2;
    this.g3 = this.arbre.g3;
    this.p2 = this.arbre.p2;
    this.p3 = this.arbre.p3;
  }

  onFilleul1Selected(id: number): void{
    this.g2 = [];
    this.g3 = [];
    this.p2 = [];
    this.p3 = [];
    this.g2 = this.arbre.g2.filter(membre => membre.fkParrain === id);
    this.p2 = this.arbre.p2.filter(membre => membre.membre.fkParrain === id);
  }

  onFilleul2Selected(id: number): void{
    this.g3 = [];
    this.g3 = this.arbre.g3.filter(membre => membre.fkParrain === id);
    this.p3 = [];
    this.p3 = this.arbre.p3.filter(membre => membre.membre.fkParrain === id);
  }

}
