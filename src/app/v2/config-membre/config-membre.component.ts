import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Login } from 'src/app/models/login.model';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-config-membre',
  templateUrl: './config-membre.component.html',
  styleUrls: ['./config-membre.component.css']
})
export class ConfigMembreComponent implements OnInit {

  constructor(
    private appConfig: AppConfigService,
    private service: GestionFilleulService,
    private dialog: MatDialog,
    private router: Router
  ) { }

  login: Login;
  ancien: string;
  ancienSaved: string;
  confirm: string

  ngOnInit(): void {
    this.login = this.appConfig.userConnected.login;
    this.login.fkMembre = this.appConfig.userConnected.id
    this.ancienSaved = this.appConfig.userConnected.login.password
  }

  onSubmit(): void{
    if(this.ancien != this.ancienSaved){
      Utility.openInfoDialog(this.dialog,'Ancien mot de passe incorrect');
      return;
    }

    if(this.confirm != this.login.password){
      Utility.openInfoDialog(this.dialog,'Les deux mots de passe ne correspondent pas');
      return;
    }

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sur de vouloir changer les paramètres?').afterClosed()
    .subscribe(
      res => {
        if(res == 'OK'){
          this.service.configLogin(JSON.stringify(this.login)).then(
            result => {
              this.appConfig.onDisconnected()
              this.router.navigate(['/login'])
            }
          ).catch(error => Utility.openInfoDialog(this.dialog, error))
        }
      }
    )

  }

}
