import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Login } from 'src/app/models/login.model';
import { AppConfigService } from 'src/app/services/app-config.service';
import { GestionFilleulService } from 'src/app/services/gestion-filleul.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-config-role-membre',
  templateUrl: './config-role-membre.component.html',
  styleUrls: ['./config-role-membre.component.css']
})
export class ConfigRoleMembreComponent implements OnInit {

  constructor(
    private appConfig: AppConfigService,
    private service: GestionFilleulService,
    private dialog: MatDialog,
    private router: Router
  ) { }

  login: Login;
  id: string = '';

  ngOnInit(): void {
  }

  onSubmit(): void{

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sur de vouloir changer le rôle?').afterClosed()
    .subscribe(
      res => {
        if(res == 'OK'){
          this.service.configLogin(JSON.stringify(this.login)).then(
            result => {
              this.login = undefined;
            }
          ).catch(error => Utility.openInfoDialog(this.dialog, error))
        }
      }
    )

  }

  search(): void{
    this.service.getMembre(JSON.stringify({telephone: this.id}))
    .then(
      res => {
        this.login = res.response.login;
        this.login.fkMembre = res.response.id;
      }
    )
  }

}
